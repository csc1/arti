/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Clipper.h"

#include <sstream>

#include <QFileDialog>
#include <QFileSystemModel>
#include <QMessageBox>
#include <QMoveEvent>
#include <QTreeView>

#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkImageChangeInformation.h>
#include <vtkImageData.h>
#include <vtkImageViewer.h>
#include <vtkMetaImageWriter.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkNew.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkVersion.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>

#if VTK_MAJOR_VERSION >= 9
#include <vtkJSONDataSetWriter.h>
#endif

/*
| Constructor.
*/
MainWindow::MainWindow( )
	: resultSize( 0 )
{
	status = STATUS_SELECT;
	ui = new Ui_MainWindow;
	ui->setupUi( this );
	clipper = new Clipper( ui );

	ui->tab->setTabEnabled( 1, false );
	ui->tab->setTabEnabled( 2, false );
	ui->tab->setTabEnabled( 3, false );
	ui->tab->setTabEnabled( 4, false );
	ui->tab->setTabEnabled( 5, false );

	ui->frameClip->layout( )->setMargin( 0 );
	ui->frameClip->layout( )->setSpacing( 0 );
	ui->frameHCenter->layout( )->setMargin( 0 );
	ui->frameHCenter->layout( )->setSpacing( 0 );
	ui->framePanelBottom->layout( )->setMargin( 0 );
	ui->framePanelBottom->layout( )->setSpacing( 0 );
	ui->framePanelBottom->setMinimumHeight( 65 );
	ui->frameVMiddle->layout( )->setMargin( 0 );
	ui->frameVMiddle->layout( )->setSpacing( 0 );
	ui->frameRaycast->layout( )->setSpacing( 0 );

	connect( ui->tab, SIGNAL( currentChanged( int ) ), this, SLOT( slotTabSelected( int ) ) );
	connect( ui->actionExit, SIGNAL( triggered( ) ), this, SLOT( slotExit( ) ) );
	connect( ui->buttonStart, SIGNAL( clicked( bool ) ), this, SLOT( slotButtonStartClicked( bool ) ) );
	connect( ui->buttonAbort, SIGNAL( clicked( bool ) ), this, SLOT( slotButtonAbortClicked( bool ) ) );
	connect( ui->buttonUse, SIGNAL( clicked( bool ) ), this, SLOT( slotButtonUseClicked( bool ) ) );
	connect( ui->buttonSaveMhd, SIGNAL( clicked( bool ) ), this, SLOT( slotButtonSaveMhdClicked( bool ) ) );
	connect( ui->buttonSaveVti, SIGNAL( clicked( bool ) ), this, SLOT( slotButtonSaveVtiClicked( bool ) ) );
	connect( ui->buttonSaveJson, SIGNAL( clicked( bool ) ), this, SLOT( slotButtonSaveJsonClicked( bool ) ) );
	connect( ui->spinUseSlice, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinUseSliceChanged( int ) ) );
	connect( ui->spinThreads, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinThreadsChanged( int ) ) );

	connect( ui->spinZStack, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinZStackChanged( int ) ) );
	connect( ui->sliderZStack, SIGNAL( valueChanged( int ) ), this, SLOT( slotSliderZStackChanged( int ) ) );

	connect( ui->spinZ1X, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinZ1XChanged( int ) ) );
	connect( ui->spinZ2X, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinZ2XChanged( int ) ) );
	connect( ui->spinZ2Y, SIGNAL( valueChanged( double ) ), this, SLOT( slotSpinZ2YChanged( double ) ) );
	connect( ui->sliderZ1X, SIGNAL( valueChanged( int ) ), this, SLOT( slotSliderZ1XChanged( int ) ) );
	connect( ui->sliderZ2X, SIGNAL( valueChanged( int ) ), this, SLOT( slotSliderZ2XChanged( int ) ) );
	connect( ui->sliderZ2Y, SIGNAL( valueChanged( int ) ), this, SLOT( slotSliderZ2YChanged( int ) ) );

	connect( clipper, SIGNAL( progress( double ) ), this, SLOT( slotReadProgress( double ) ) );
	connect( clipper, SIGNAL( aborted( ) ), this, SLOT( slotClipperAborted( ) ) );
	connect( clipper, SIGNAL( finished( ) ), this, SLOT( slotClipperFinished( ) ) );
	connect( &iradon, SIGNAL( progress( int ) ), this, SLOT( slotIRadonProgress( int ) ) );
	connect( &iradon, SIGNAL( aborted( ) ), this, SLOT( slotIRadonAborted( ) ) );
	connect( &iradon, SIGNAL( finished( ) ), this, SLOT( slotIRadonFinished( ) ) );

	// FIXME overload QFileSystemModel to not show expansion arrows on empty folders
	inputModel = new QFileSystemModel;
	inputModel->setRootPath( QDir::rootPath( ) );
	inputModel->setReadOnly( true );
	inputModel->setFilter( QDir::Dirs | QDir::Drives | QDir::NoDotAndDotDot | QDir::AllDirs );

	QTreeView * tree = ui->treeView;
	tree->setModel( inputModel );
	tree->setRootIndex( inputModel->index( QDir::rootPath( ) ) );
	{
		QDir dir = QDir::home( );
		for(;;)
		{
			tree->setExpanded( inputModel->index( dir.path( ) ), true );
			if( !dir.cdUp( ) ) break;
		}
	}
	tree->setSortingEnabled( true );
	tree->sortByColumn( 0, Qt::AscendingOrder );
	for( int i = 1; i < inputModel->columnCount( ); i++ ) tree->hideColumn( i );

	connect(
		tree->selectionModel( ), SIGNAL( selectionChanged( const QItemSelection &, const QItemSelection & ) ),
		this, SLOT( slotSelectionChanged( const QItemSelection &, const QItemSelection & ) )
	);

	QString selectionBase = QString( "" );
	int selectionCount = 0;

	vImageChangeZStack = vtkSmartPointer<vtkImageChangeInformation>::New( );
	vImageViewerZStack = vtkSmartPointer<vtkImageViewer>::New( );
	vImageViewerZStack->SetInputConnection( vImageChangeZStack->GetOutputPort( ) );
	vImageViewerZStack->GetRenderer( )->SetBackground( 1.0, 1.0, 1.0 );
	vRenderWindowZStack = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New( );
	ui->vtkZStack->setRenderWindow( vRenderWindowZStack );
	vRenderWindowZStack->SetWindowName( "zstack" );
	vRenderWindowZStack->AddRenderer( vImageViewerZStack->GetRenderer( ) );
	vPFuncRay = vtkSmartPointer<vtkPiecewiseFunction>::New( );

	vVolume = vtkSmartPointer<vtkVolume>::New( );
	vRendererRay = vtkSmartPointer<vtkRenderer>::New( );
	vRendererRay->SetBackground( 1.0, 1.0, 1.0 );
	vVolumeMapper = vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New( );
	vVolume->SetMapper( vVolumeMapper );

	vtkNew<vtkVolumeProperty> volumeProperty;
	volumeProperty->SetScalarOpacity( vPFuncRay );
	volumeProperty->SetInterpolationTypeToLinear( );
	vVolume->SetProperty( volumeProperty );
	vRendererRay->AddVolume( vVolume );

#if VTK_MAJOR_VERSION >= 9
	ui->buttonSaveJson->setEnabled( true );
#endif
}


/*
| Destructor.
*/
MainWindow::~MainWindow( )
{
	if( ui ){ delete ui; ui = 0; }
	if( inputModel ){ delete inputModel; inputModel = 0; }
}


/*
| Abort button clicked.
*/
void MainWindow::slotButtonAbortClicked( bool )
{
	switch( status )
	{
		case STATUS_READING :
			clipper->abortData( );
			break;
		case STATUS_COMPUTING :
			iradon.abort( );
			break;
		default : return; // ignore when not knowing what to do
	}
	ui->buttonAbort->setEnabled( false );
	ui->labeliRadonStatus->setText( "aborting.." );
	ui->labeliRadonStatus->setStyleSheet( "#labeliRadonStatus{ color : red; }" );
}

/*
| Start button clicked.
*/
void MainWindow::slotButtonStartClicked( bool )
{
	status = STATUS_READING;
	ui->buttonAbort->setEnabled( true );
	ui->buttonStart->setEnabled( false );
	ui->labeliRadonStatus->setText( "computing!" );
	ui->labeliRadonStatus->setStyleSheet( "#labeliRadonStatus{ color : orange; }" );
	ui->spinUseSlice->setEnabled( false );
	ui->spinOutputSize->setEnabled( false );
	ui->spinThreads->setEnabled( false );
	ui->buttonUse->setEnabled( true );

	ui->progressRead->setValue( 0 );
	ui->progressCompute->setValue( 0 );
	clipper->requestData( ui->spinUseSlice->value( ) );
}

/*
| Quits.
*/
void MainWindow::slotExit()
{
	qApp->exit();
}

/*
| IRadon progress report.
*/
void MainWindow::slotIRadonProgress( int counter )
{
	ui->progressCompute->setValue( counter );
}

/*
| Clipper read progress report.
*/
void MainWindow::slotReadProgress( double amount )
{
	ui->progressRead->setValue( 100 * amount );
}

/*
| Clipper finished reading.
*/
void MainWindow::slotClipperFinished(  )
{
	int slices = clipper->dataDims[ 1 ];
	ui->progressCompute->setMaximum( slices );
	{
		std::stringstream s( "%p /" );
		s << slices;
		ui->progressCompute->setFormat( QString( "\%v / %1" ).arg( slices ) );
	}
	status = STATUS_COMPUTING;
	int os = ui->spinOutputSize->value( ) * ui->spinPrescaling->value( );
	// round up to next 4 devideable
	if( os % 4 ) os += 4 - ( os % 4 );
	iradon.setResultSize( os );
	double theta = ui->spinTheta->value( ) * M_PI / 180.0;
	iradon.start( clipper->data, clipper->dataDims, theta );
}

/*
| Clipper finished aborting.
*/
void MainWindow::slotClipperAborted(  )
{
	ui->labeliRadonStatus->setText( "idle" );
	ui->labeliRadonStatus->setStyleSheet( "#labeliRadonStatus{ color : black; }" );
	ui->progressRead->setValue( 0 );
	ui->progressCompute->setValue( 0 );
	ui->buttonStart->setEnabled( true );
	ui->spinUseSlice->setEnabled( true );
	ui->spinOutputSize->setEnabled( true );
	ui->spinThreads->setEnabled( true );
	ui->buttonUse->setEnabled( true );
	status = STATUS_CLIP;
}

/*
| IRadon finished.
*/
void MainWindow::slotIRadonFinished(  )
{
	vtkNew<vtkImageData> irandData;
	resultSize = iradon.getResultSize( );
	int h = iradon.getResultHeight( );

	irandData->SetDimensions( resultSize, resultSize, h );
	irandData->AllocateScalars( VTK_FLOAT, 1 );
	memcpy( irandData->GetScalarPointer( ), iradon.result, resultSize * resultSize * h * sizeof( float ) );
	{
		// zstack
		vImageChangeZStack->SetInputData( irandData );
		vImageViewerZStack->SetZSlice( h / 2 );
		vImageViewerZStack->SetColorLevel( 255.0 / 2 );
		vImageViewerZStack->SetColorWindow( 255.0 );

		ui->vtkZStack->renderWindow( )->Render( );

		ui->spinZStack->blockSignals( true );
		ui->sliderZStack->blockSignals( true );
		ui->spinZStack->setMaximum( h - 1);
		ui->sliderZStack->setMaximum( h - 1);
		ui->spinZStack->setValue( h / 2 );
		ui->sliderZStack->setValue( h / 2 );
		ui->sliderZStack->blockSignals( false );
		ui->spinZStack->blockSignals( false );
	}

	{
		// raycaster
		vPFuncRay->RemoveAllPoints( );
		vPFuncRay->AddPoint( 0, 0.0 );
		vPFuncRay->AddPoint( 6, 0.0 );
		vPFuncRay->AddPoint( 255, 0.4 );

		{
			int threads = ui->spinThreads->value( ) / 2;
			if( threads < 1 ) threads = 1;
			vVolumeMapper->SetNumberOfThreads( threads ) ;
		}
		vVolumeMapper->SetInputData( irandData );

		vRendererRay->ResetCamera( );
		ui->vtkRaycast->renderWindow( )->AddRenderer( vRendererRay );
		ui->vtkRaycast->renderWindow( )->SetWindowName( "raycast" );
	}

	ui->buttonAbort->setEnabled( false );
	ui->buttonStart->setEnabled( true );
	ui->labeliRadonStatus->setText( "done!" );
	ui->labeliRadonStatus->setStyleSheet( "#labeliRadonStatus{ color : black; }" );
	ui->spinUseSlice->setEnabled( true );
	ui->spinThreads->setEnabled( true );
	ui->spinOutputSize->setEnabled( true );
	ui->buttonUse->setEnabled( true );
	ui->tab->setTabEnabled( 3, true );
	ui->tab->setTabEnabled( 4, true );
	ui->tab->setTabEnabled( 5, true );
	status = STATUS_DONE;
}

/*
| IRadon finished aborting.
*/
void MainWindow::slotIRadonAborted(  )
{
	ui->labeliRadonStatus->setText( "idle" );
	ui->labeliRadonStatus->setStyleSheet( "#labeliRadonStatus{ color : black; }" );
	ui->progressRead->setValue( 0 );
	ui->progressCompute->setValue( 0 );
	ui->buttonStart->setEnabled( true );
	ui->buttonUse->setEnabled( true );
	ui->spinUseSlice->setEnabled( true );
	ui->spinOutputSize->setEnabled( true );
	ui->spinThreads->setEnabled( true );
	status = STATUS_CLIP;
}

/*
| The input selector changed.
*/
void
MainWindow::slotSelectionChanged( const QItemSelection &, const QItemSelection & )
{
	QTreeView * tree = ui->treeView;
	QString base = inputModel->filePath( tree->currentIndex( ) );
	selectionBase = base + QString( "/" );
	QString dotJpg( ".jpg" );
	selectionCount = 0;

	for(;;)
	{
		QString number = QString( "%1" ).arg( selectionCount + 1, 4, 10, QChar('0') );
		QString testPath = selectionBase + number + dotJpg;
		QFile file( testPath );
		if( !file.exists( ) ) break;
		selectionCount++;
	}

	if( selectionCount == 0 )
	{
		ui->labelInput->setText( "nothing suitable" );
		ui->buttonUse->setEnabled ( false );
		return;
	}

	ui->labelInput->setText( QString( "found %1 images" ).arg( selectionCount ) );
	ui->buttonUse->setEnabled ( true );
}

/*
| Window has been resized.
*/
void
MainWindow::resizeEvent( QResizeEvent* event )
{
	QMainWindow::resizeEvent( event );
	clipper->update( );
	updateZStack( );
}

/*
| Handles window maximize/restore change.
*/
void
MainWindow::changeEvent( QEvent* evt )
{
	QMainWindow::changeEvent( evt );
	if( evt->type( ) == QEvent::WindowStateChange )
	{
		clipper->update( );
		updateZStack( );
	}
}

void
MainWindow::slotButtonUseClicked( bool )
{
	status = STATUS_CLIP;
	ui->tab->setTabEnabled( 1, true );
	ui->tab->setTabEnabled( 2, true );
	ui->tab->setCurrentIndex( 1 );
	clipper->init( selectionBase, selectionCount );
}

void
MainWindow::slotButtonSaveMhdClicked( bool )
{
	QString fileName =
		QFileDialog::getSaveFileName( this, "Save File", "", ".mhd" );

	vtkNew<vtkImageData> iData;
	resultSize = iradon.getResultSize( );
	int h = iradon.getResultHeight( );

	iData->SetDimensions( resultSize, resultSize, h );
	iData->AllocateScalars( VTK_UNSIGNED_CHAR, 1 );
	unsigned char * d = static_cast<unsigned char *>( iData->GetScalarPointer( ) );
	int p = 0;
	int totalSize = resultSize * resultSize * h;
	const float * f = iradon.result;

	for( int p = 0; p < totalSize; p++ ) d[ p ]  = f[ p ];

	vtkNew<vtkMetaImageWriter> writer;
	writer->SetInputData( iData );
	writer->SetFileName( fileName.toStdString( ).c_str( ) );
	writer->SetCompression( false );
	writer->Write( );

	QMessageBox msgBox;
	msgBox.setText( "Done" );
	msgBox.exec();
}

void
MainWindow::slotButtonSaveVtiClicked( bool )
{
	QString fileName =
		QFileDialog::getSaveFileName( this, "Save File", "", ".vti" );

	vtkNew<vtkImageData> iData;
	resultSize = iradon.getResultSize( );
	int h = iradon.getResultHeight( );

	iData->SetDimensions( resultSize, resultSize, h );
	iData->AllocateScalars( VTK_UNSIGNED_CHAR, 1 );
	unsigned char * d = static_cast<unsigned char *>( iData->GetScalarPointer( ) );
	int p = 0;
	int totalSize = resultSize * resultSize * h;
	const float * f = iradon.result;

	for( int p = 0; p < totalSize; p++ ) d[ p ]  = f[ p ];

	vtkNew<vtkXMLImageDataWriter> writer;
	writer->SetInputData( iData );
	writer->SetFileName( fileName.toStdString( ).c_str( ) );
	writer->Write( );

	QMessageBox msgBox;
	msgBox.setText( "Done" );
	msgBox.exec();
}

void
MainWindow::slotButtonSaveJsonClicked( bool )
{
#if VTK_MAJOR_VERSION >= 9
	QString fileName =
		QFileDialog::getSaveFileName( this, "Save File", "", "" );

	vtkNew<vtkImageData> iData;
	resultSize = iradon.getResultSize( );
	int h = iradon.getResultHeight( );

	iData->SetDimensions( resultSize, resultSize, h );
	iData->AllocateScalars( VTK_UNSIGNED_CHAR, 1 );
	unsigned char * d = static_cast<unsigned char *>( iData->GetScalarPointer( ) );
	int p = 0;
	int totalSize = resultSize * resultSize * h;
	const float * f = iradon.result;

	for( int p = 0; p < totalSize; p++ ) d[ p ]  = f[ p ];

	vtkNew<vtkJSONDataSetWriter> writer;
	writer->SetInputData( iData );
	writer->SetFileName( fileName.toStdString( ).c_str( ) );
	writer->Write( );

	QMessageBox msgBox;
	msgBox.setText( "Done" );
	msgBox.exec();
#endif
}

void
MainWindow::slotSpinUseSliceChanged( int value )
{
	switch( value )
	{
		case 1 : ui->labelUseSlice->setText( "slice" ); break;
		case 2 : ui->labelUseSlice->setText( "nd slice" ); break;
		case 3 : ui->labelUseSlice->setText( "rd slice" ); break;
		default : ui->labelUseSlice->setText( "th slice" ); break;
	}
}

void
MainWindow::slotSpinThreadsChanged( int value )
{
	iradon.setNumThreads( value );
}

void
MainWindow::slotSpinZ1XChanged( int value )
{
	ui->sliderZ1X->blockSignals( true );
	ui->sliderZ1X->setValue( value );
	ui->sliderZ1X->blockSignals( false );

	if( ui->spinZ2X->value( ) <= value )
	{
		ui->spinZ2X->blockSignals( true );
		ui->sliderZ2X->blockSignals( true );
		ui->spinZ2X->setValue( value + 1 );
		ui->sliderZ2X->setValue( value + 1 );
		ui->sliderZ2X->blockSignals( false );
		ui->spinZ2X->blockSignals( false );
	}
	updateRayFunc( );
}

void
MainWindow::slotSpinZ2XChanged( int value )
{
	if( value <= ui->spinZ1X->value( ) )
	{
		value = ui->spinZ1X->value( ) + 1;
		ui->spinZ2X->blockSignals( true );
		ui->spinZ2X->setValue( value );
		ui->spinZ2X->blockSignals( false );
	}
	ui->sliderZ2X->blockSignals( true );
	ui->sliderZ2X->setValue( value );
	ui->sliderZ2X->blockSignals( false );
	updateRayFunc( );
}

void
MainWindow::slotSpinZ2YChanged( double value )
{
	ui->sliderZ2Y->blockSignals( true );
	ui->sliderZ2Y->setValue( value * 1000 );
	ui->sliderZ2Y->blockSignals( false );
	updateRayFunc( );
}

void
MainWindow::slotSliderZ1XChanged( int value )
{
	ui->spinZ1X->blockSignals( true );
	ui->spinZ1X->setValue( value );
	ui->spinZ1X->blockSignals( false );
	if( ui->spinZ2X->value( ) <= value )
	{
		ui->spinZ2X->blockSignals( true );
		ui->sliderZ2X->blockSignals( true );
		ui->spinZ2X->setValue( value + 1 );
		ui->sliderZ2X->setValue( value + 1 );
		ui->sliderZ2X->blockSignals( false );
		ui->spinZ2X->blockSignals( false );
	}
	updateRayFunc( );
}

void
MainWindow::slotSliderZ2XChanged( int value )
{
	if( value <= ui->spinZ1X->value( ) )
	{
		value = ui->spinZ1X->value( ) + 1;
		ui->sliderZ2X->blockSignals( true );
		ui->sliderZ2X->setValue( value );
		ui->sliderZ2X->blockSignals( false );
	}
	ui->spinZ2X->blockSignals( true );
	ui->spinZ2X->setValue( value );
	ui->spinZ2X->blockSignals( false );
	updateRayFunc( );
}

void
MainWindow::slotSliderZ2YChanged( int value )
{
	ui->spinZ2Y->blockSignals( true );
	ui->spinZ2Y->setValue( value / 1000.0 );
	ui->spinZ2Y->blockSignals( false );
	updateRayFunc( );
}

void
MainWindow::updateRayFunc( )
{
	vPFuncRay->RemoveAllPoints( );
	vPFuncRay->AddPoint( 0, 0.0 );
	vPFuncRay->AddPoint( ui->spinZ1X->value( ), 0.0 );
	vPFuncRay->AddPoint( ui->spinZ2X->value( ), ui->spinZ2Y->value( ) );
	ui->vtkRaycast->renderWindow( )->Render( );
}

void
MainWindow::slotSpinZStackChanged( int value )
{
	ui->sliderZStack->blockSignals( true );
	ui->sliderZStack->setValue( value );
	ui->sliderZStack->blockSignals( false );
	updateZStack( );
}

void
MainWindow::slotSliderZStackChanged( int value )
{
	ui->spinZStack->blockSignals( true );
	ui->spinZStack->setValue( value );
	ui->spinZStack->blockSignals( false );
	updateZStack( );
}

void
MainWindow::updateZStack( )
{
	if( !resultSize ) return;

	vImageViewerZStack->SetZSlice( ui->spinZStack->value( ) );
	int * size = vRenderWindowZStack->GetScreenSize( );

	int sx = size[ 0 ];
	int sy = size[ 1 ];

	if( sx > sy )
	{
		int oy = resultSize;
		int ox = (double) sx / (double) sy * (double) resultSize;
		vRenderWindowZStack->SetSize( ox, oy );
		vImageChangeZStack->SetOutputExtentStart( ( ox - resultSize ) / 2, 0, 0 );
	}
	else
	{
		int ox = resultSize;
		int oy = (double) resultSize / sx * sy;
		vRenderWindowZStack->SetSize( ox, oy );
		vImageChangeZStack->SetOutputExtentStart( 0, ( oy - resultSize ) / 2, 0 );
	}

	ui->vtkZStack->renderWindow( )->Render( );
}

void
MainWindow::slotTabSelected( int tab )
{
	if( tab == 1 ) clipper->update( );
	if( tab == 3 ) updateZStack( );
}
