/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#ifndef IRADON_WORKER_H
#define IRADON_WORKER_H

#include <QObject>

#include "IRadon.h"
#include "ikern/IKern.h"


/*
| Threaded workers for the IRadon class.
*/
class IRadonWorker : public QObject {
	Q_OBJECT

public:

	IRadonWorker(
		int id,                      // the workers id
		int y0,                      // from this slice
		int y1,                      // to this slice
		int w,                       // width
		int h,                       // height
		int wp2,                     // width next power of 2
		int z,                       // amount of angle steps
		unsigned char const * raw,   // input
		int pw,                      // padding area width
		int const wso,               // signal offset in pads
		int const wfo,               // signal enlarged by power 2 offset
		float * result,              // output
		int resultSize,              // output size ( width and height )
		IKern * kern,                // kernel
		enum IRadon::interp_e interp // interpolation type
	) :
		id( id ),
		y0( y0 ),
		y1( y1 ),
		w( w ),
		h( h ),
		wp2( wp2 ),
		z( z ),
		raw( raw ),
		pw( pw ),
		wso( wso ),
		wfo( wfo ),
		result( result ),
		resultSize( resultSize ),
		kern( kern ),
		interp( interp ),
		aborting( false )
	{ }

	void abort( );
	~IRadonWorker( );

public slots:

	void process( );

signals:
	void finished( int id );
	void aborted( int id );
	void step( );

private:
	int id;
	int y0;
	int y1;
	int w;
	int h;
	int wp2;
	int z;
	unsigned char const * raw;
	int pw;
	int const wso;
	int const wfo;
	float * result;
	int resultSize;
	IKern * kern;
	bool aborting;
	enum IRadon::interp_e interp;
};

#endif
