/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#ifndef CLIPPER_H
#define CLIPPER_H

#include <vtkSmartPointer.h>
#include <vtkImageCanvasSource2D.h>
#include <vtkImageChangeInformation.h>
#include <vtkImageClip.h>
#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkImageShiftScale.h>
#include <vtkImageViewer.h>
#include <vtkInteractorStyle.h>
#include <vtkInteractorStyleUser.h>
#include <vtkJPEGReader.h>

#include <QSplitter>
#include <QVTKOpenGLNativeWidget.h>


class Ui_MainWindow;
class ClipperWorker;

/*
| Handles the clipper stuff in the main window.
*/
class Clipper : public QObject, vtkInteractorStyle
{
	Q_OBJECT

public:
	Clipper( Ui_MainWindow * ui );
	~Clipper( );

	void update( );
	void init( QString const & path, int nSlices );

	void requestData( int every );
	void abortData( );

	void OnChar( ) override;
	void OnMouseMove( ) override;
	void OnLeftButtonDown( ) override;
	void OnLeftButtonUp( ) override;
	void OnRightButtonDown( ) override;
	void OnRightButtonUp( ) override;

	// fully read, clipped data
	unsigned char * clipData = 0;

	// true if the output size is explicitly set by user
	// and thus not to be autoset.
	bool outputSizeUserSet = false;

	int dataDims[ 3 ] = { 0, };
	unsigned char * data = 0; // the data here when finished

public slots:

	void slotCheckHelpersChanged( int );
	void slotCheckInvertChanged( int );
	void slotCurSliceChanged( int );
	void slotHSplitterMoved( int, int );
	void slotDialRotateChanged( int );
	void slotDialRotateKeyPressed( int );
	void slotSpinSizeChanged( int );
	void slotSpinCenterChanged( int );
	void slotSpinOutputSizeChanged( int );
	void slotSpinRotateChanged( double );
	void slotVSplitterMoved( int, int );
	void slotWorkerAborted( );
	void slotWorkerFinished( );
	void slotWorkerProgress( double );

signals:
	void progress( double amount );
	void aborted( );
	void finished( );

private:
	Ui_MainWindow * ui;

	// if false the clipper isn't initialized
	bool loaded = false;

	QString srcPath;
	int nSlices;
	int zSlice = 0;
	bool leftMousePressed = false;
	bool rightMousePressed = false;
	int pressStartX = 0;
	int pressStartY = 0;
	int originX = 0;
	int originY = 0;
	int imageW = 0;
	int imageH = 0;
	int clipW = 0;
	int clipH = 0;

	// pointer to the clipper widget
	QVTKOpenGLNativeWidget * clipperWidget = 0;
	ClipperWorker * worker = 0;
	QThread * thread = 0;

	// various pointers to the clipping vtk chain
	vtkSmartPointer<vtkJPEGReader> vReader;
	vtkSmartPointer<vtkImageClip> vClip;
	vtkSmartPointer<vtkImageChangeInformation> vImageChange;
	vtkSmartPointer<vtkImageShiftScale> vInvert;
	vtkSmartPointer<vtkInteractorStyleUser> interactorUser;
	vtkSmartPointer<vtkImageCanvasSource2D> vLineDrawing;
	vtkSmartPointer<vtkImageReslice> vReslice;
	vtkSmartPointer<vtkImageViewer> vImageViewer;

	void leftMouseAction( int x, int y, bool release );
	void rightMouseAction( int x, int y, bool release );
	int splitterMoved( QSplitter * splitter, int pos, int index );
	void updateVtkClipper( int ox, int oy, int oz );
	void setClipperSize( int w, int h );
	void updateRotate( double angle );
};

#endif
