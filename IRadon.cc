/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#include <assert.h>
#include <iostream>

#include <QThread>

#include "IRadon.h"
#include "IRadonWorker.h"
#include "ikern/IKern.h"
#include "kiss_fft130/kiss_fft.h"

using namespace std;

/*
| Constructor.
*/
IRadon::IRadon( )
{
	//interp = INTERP_NEAREST;
	interp = INTERP_LINEAR;
	kernel = KERNEL_PLAIN;
	workers = 0;
	kern = 0;
	result = 0;
	if( __builtin_cpu_supports( "sse4.2" ) ) kernel = KERNEL_SSE42;
	if( __builtin_cpu_supports( "avx2" ) ) kernel = KERNEL_AVX2;
	cout << "using iradon kernel: " << kernel << endl;
}

/*
| Destructor.
*/
IRadon::~IRadon( )
{
	if( result ){ delete[ ] result; result = 0; }
	if( kern ){ delete kern; kern = 0; }
	if( workers ){ delete[ ] workers; workers = 0; }
}

/*
| A worker aborted
*/
void
IRadon::slotWorkerAborted( int id )
{
	numWorkers--;
	workers[ id ] = 0;
	if( numWorkers == 0 )
	{
		emit aborted( );
	}
}

/*
| A worker finished.
*/
void
IRadon::slotWorkerFinished( int id )
{
	numWorkers--;
	workers[ id ] = 0;
	if( numWorkers != 0 ) return;

	float max = 0;
	// FIXME rename variables
	for( int y = 0; y < resultHeight; y++ )
	{
		for( int ix = 0; ix < resultSize; ix++ )
		{
			for( int iy = 0; iy < resultSize; iy++ )
			{
				float v = result[ y * resultSize * resultSize + iy * resultSize + ix ];
				if( v > max ) max = v;
			}
		}
	}
	for( int y = 0; y < resultHeight; y++ )
	{
		for( int ix = 0; ix < resultSize; ix++ )
		{
			for( int iy = 0; iy < resultSize; iy++ )
			{
				int p = y * resultSize * resultSize + iy * resultSize + ix;
				float v = result[ p ];
				v = v * 255.0 / max;
				if( v < 0 ) v = 0;
				result[ p ] = v;
			}
		}
	}
	emit finished( );
}

/*
| A worker finished on slice.
*/
void
IRadon::slotWorkerStep( )
{
	progressCounter++;
	emit progress( progressCounter );
}

/*
| Starts the computation.
*/
void
IRadon::start(
	unsigned char const * idata, // pointer to input data
	int const * idataDims,       // dimensions of idata
	double theta                 // theta in radiant
)
{
	int const w = idataDims[ 0 ];
	int const h = idataDims[ 1 ];
	int const z = idataDims[ 2 ];
	resultHeight = h;
	aborting = false;

	if( kern ) delete kern;
	switch( kernel )
	{
		case KERNEL_PLAIN : kern = new IKernPlain( z, theta ); break;
		case KERNEL_SSE42 : kern = new IKernSSE42( z, theta ); break;
		case KERNEL_AVX2 : kern = new IKernAVX2( z, theta ); break;
		default : assert( false );
	}

	// width on next power of 2
	int wp2 = 2;
	for( int wi = w; wi > 1; wp2 *= 2, wi /= 2 );

	int const wp2h = wp2 / 2;
	int const resultSize05 = resultSize / 2;

	// next power of 2 * sqrt( 2 ) (max value of sin(x)*cos(x))
	int const issq2 = ceil( resultSize * sqrt( 2 ) );

	int const pw = max( wp2, issq2 + 2 );
	int const pw05 = pw / 2;

	// signal offset in pads
	int const wso = ( pw - w ) / 2;

	// signal enlarged by power 2 offset (for fft)
	int const wfo = ( pw - wp2 ) / 2;

	result = new float[ resultSize * resultSize * h ]( );
	progressCounter = 0;
	numWorkers = numThreads;
	if( workers ) delete[ ] workers;
	workers = new IRadonWorker*[ numThreads ];
	int tslices = h / numThreads;
	if( kernel != KERNEL_PLAIN ) tslices -= tslices % 4;

	for( int i = 0; i < numThreads; i++ )
	{
		int y0 = i * tslices;
		int y1;
		if( i + 1 < numThreads ) y1 = y0 + tslices; else y1 = h;

		QThread * thread = new QThread;
		IRadonWorker * worker = new IRadonWorker(
			i,
			y0, y1,
			w, h,
			wp2, z,
			idata,
			pw, wso, wfo,
			result, resultSize,
			kern, interp
		);

		workers[ i ] = worker;
		worker->moveToThread( thread );

		connect( thread, SIGNAL( started( ) ), worker, SLOT( process( ) ) );
		connect( worker, SIGNAL( finished( int ) ), thread, SLOT( quit( ) ) );
		connect( worker, SIGNAL( finished( int ) ), worker, SLOT( deleteLater( ) ) );
		connect( worker, SIGNAL( finished( int ) ), this, SLOT( slotWorkerFinished( int ) ) );
		connect( worker, SIGNAL( step( ) ), this, SLOT( slotWorkerStep( ) ) );
		connect( worker, SIGNAL( aborted( int ) ), this, SLOT( slotWorkerAborted( int ) ) );
		connect( thread, SIGNAL( finished( ) ), thread, SLOT( deleteLater( ) ) );

		thread->start( );
	}
}

/*
| Forwards the abort request to all workers.
*/
void
IRadon::abort( )
{
	for( int i = 0; i < numThreads; i++ )
	{
		IRadonWorker * w = workers[ i ];
		if( w ) w->abort( );
	}
}
