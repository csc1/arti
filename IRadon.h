/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#ifndef IRADON_H
#define IRADON_H

#include <atomic>

#include <QObject>

#include <vtkImageData.h>
#include <vtkSmartPointer.h>

#include "ikern/IKern.h"


/*
| Calculates the inverse radon transformation.
*/
class IRadonWorker;
class IRadon : public QObject
{
	Q_OBJECT

public:
	enum kernel_e {
		KERNEL_PLAIN,
		KERNEL_SSE42,
		KERNEL_AVX2
	};

	enum interp_e {
		INTERP_NEAREST,
		INTERP_LINEAR
	};

	float * result = 0;

	IRadon( );
	~IRadon( );

	void setKernel( enum kernel_e kernel ) { this->kernel = kernel; }
	void setNumThreads( int numThreads ) { this->numThreads = numThreads; }
	void setResultSize( int resultSize ) { this->resultSize = resultSize; }
	void setInterp( enum interp_e interp ) { this->interp = interp; }

	// Starts the computation
	void start( unsigned char const *, int const *, double );

	int getResultSize( ) { return resultSize; }
	int getResultHeight( ) { return resultHeight; }
	void abort( );

public slots:

	void slotWorkerAborted( int id );
	void slotWorkerFinished( int id );
	void slotWorkerStep( );

signals:
	void progress( int counter );
	void aborted( );
	void finished( );

private:
	IRadonWorker** workers = 0;
	IKern * kern = 0;
	int numThreads = 8;
	int numWorkers = 0;
	bool running = false;
	int resultSize = 0;
	int resultHeight = 0;
	enum kernel_e kernel;
	enum interp_e interp;
	int progressCounter = 0;
	bool aborting = false;
};


#endif
