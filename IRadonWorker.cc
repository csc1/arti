/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#include <assert.h>
#include <iostream>

#include "IRadonWorker.h"
#include "ikern/IKern.h"
#include "kiss_fft130/kiss_fft.h"

using namespace std;


IRadonWorker::~IRadonWorker( )
{
}

void
IRadonWorker::process( )
{
	// copies thread params onto stack
	int const y0 = this->y0;                           // from this slice
	int const y1 = this->y1;                           // to this slice
	int const w = this->w;                             // width
	int const h = this->h;                             // height
	int const wp2 = this->wp2;                         // width next power of 2
	int const z = this->z;                             // amount of angle steps
	unsigned char const * const raw = this->raw;       // input
	int const pw = this->pw;   					       // padding area width
	int const wso = this->wso;                         // signal offset in pads
	int const wfo = this->wfo;                         // signal enlarged by power 2 offset
	float * const result = this->result;               // output
	int const resultSize = this->resultSize;           // output size ( width and height )
	IKern * const kern = this->kern;                   // kernel
	enum IRadon::interp_e const interp = this->interp; // interpretation type

	int const wp2h = wp2 / 2;
	int const resultSize05 = resultSize / 2;
	int const pw05 = pw / 2;

	kiss_fft_cpx * fft = new kiss_fft_cpx[ z * wp2 ]( );
	kiss_fft_cfg fft_forw = kiss_fft_alloc( wp2, 0, 0, 0 );
	kiss_fft_cfg fft_back = kiss_fft_alloc( wp2, 1, 0, 0 );
	kiss_fft_cpx * pads = new kiss_fft_cpx[ z * pw ]( );
	float * rpads = new float[ z * pw ]( );

	for( int y = y0; y < y1; y++ )
	{
		memset( pads, 0, pw * z * sizeof( *pads ) );
		for( int x = 0; x < w; x++ )
		{
			for( int t = 0; t < z; t++ )
			{
				int p = t * pw + x + wso;
				pads[ p ].r = raw[ ( t * h + y ) * w + x ];
				pads[ p ].i = 0;
			}
		}

		for( int t = 0; t < z; t++ )
		{
			kiss_fft( fft_forw, pads + t * pw + wfo, fft + t * wp2 );
		}

		for( int x = 0; x < wp2h; x++ )
		{
			float filter = float( x ) / wp2h;

			filter = sin( filter * M_PI );

			for( int t = 0; t < z; t++ )
			{
				kiss_fft_cpx * f = fft + t * wp2 + x;
				f->r *= filter;
				f->i *= filter;

				f += wp2 - 2 * x - 1;
				f->r *= filter;
				f->i *= filter;
			}
		}

		for( int t = 0; t < z; t++ )
		{
			kiss_fft( fft_back, fft + t * wp2, pads + t * pw + wfo );
		}

		for( int t = 0; t < z; t++ )
		{
			for( int w = 0; w < pw; w++ )
			{
				int p = t * pw + w;
				rpads[ p ] = pads[ p ].r;
			}
		}

		float * slice = result + y * resultSize * resultSize;
		switch( interp )
		{
			case IRadon::INTERP_NEAREST:
				kern->nearest( resultSize05, z, pw, rpads + pw05, slice );
				break;

			case IRadon::INTERP_LINEAR:
				kern->linear( resultSize05, z, pw, rpads + pw05, slice );
				break;

			default:
				assert( false );
		}

		if( aborting ) break;

		emit step( );
	}

	free( fft_forw );
	free( fft_back );
	delete[] pads;
	delete[] rpads;
	delete[] fft;

	if( aborting )
	{
		emit aborted( id );
	}
	else
	{
		emit finished( id );
	}
}


/*
| Received request to abort.
*/
void IRadonWorker::abort( )
{
	aborting = true;
}

