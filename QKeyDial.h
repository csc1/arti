/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#ifndef QKEYDIAL_H
#define QKEYDIAL_H

#include <QDial>

/*
| Just extends QDial to be sending signals on keyPressEvents.
|
| Needed by rotation dial to have quick slide access
| beginning/quoter/middle/three quarters and end slices.
*/
class QKeyDial : public QDial
{
	Q_OBJECT

public:
	QKeyDial( QWidget*& w )
		: QDial( w )
	{ }

	virtual void keyPressEvent( QKeyEvent *event ) override;

signals:
	void keyPressed( int );

};

#endif
