/*
| IRadon transformation kernel.
| Simple plain C implemenation without any
| advanced assembler instruction set tricks.
*/
#include<cmath>
#include "IKern.h"

/*
| Constructor.
*/
IKernPlain::IKernPlain( int z, float angle )
{
	makeSinTable( z, angle );
	makeCosTable( z, angle );
}

/*
| Destructor.
*/
IKernPlain::~IKernPlain( )
{
	delete[ ] pst;
	delete[ ] pct;
}

void
IKernPlain::makeSinTable( int z, float angle )
{
	float * tab = new float[ z ];
	for( int t = 0; t < z; t++ )
	{
		float phi  = (float)( t ) * angle / z;
		tab[ t ] = sin( phi );
	}
	this->pst = tab;
}

void
IKernPlain::makeCosTable( int z, float angle )
{
	float * tab = new float[ z ];
	for( int t = 0; t < z; t++ )
	{
		float phi  = (float)( t ) * angle / z;
		tab[ t ] = cos( phi );
	}
	this->pct = tab;
}

void
IKernPlain::nearest(
	int isize05,       // size of inverse radon field / 2
	int z,             // amount of steps
	int h,             // height of input
	float * rpadSh05,  // padded, filtered input, shifted by half height
	float * slice      // output
)
{
	for( int iy = -isize05; iy < isize05; iy++ )
	{
		for( int ix = -isize05; ix < isize05; ix++ )
		{
			float * p = rpadSh05;
			float const * pc = pct;
			float const * ps = pst;
			float v = 0;
			for( int t = 0; t < z; t++ )
			{
				float pf = ix * *(pc++) + iy * *(ps++);
				int pi = round( pf );
				v += p[ pi ];
				p += h;
			}
			*(slice++) = v;
		}
	}
}


void
IKernPlain::linear(
	int isize05,       // size of inverse radon field / 2
	int z,             // amount of steps
	int h,             // height of input
	float * rpadSh05,  // padded, filtered input, shifted by half height
	float * slice      // output
)
{
	for( int iy = -isize05; iy < isize05; iy++ )
	{
		for( int ix = -isize05; ix < isize05; ix++ )
		{
			float * p = rpadSh05;
			float const * pc = pct;
			float const * ps = pst;
			float v = 0;
			for( int t = 0; t < z; t++ )
			{
				float pf = ix * *(pc++) + iy * *(ps++);
				int pi = floor( pf );
				float w = pf - pi;
				v += ( 1 - w ) * p[ pi ] + w * p[ pi + 1 ];
				p += h;
			}
			*(slice++) = v;
		}
	}
}
