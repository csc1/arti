/*
| IRadon transformation kernel.
|
| Takes use of SSE42 and AVX2 instructions.
|
| This implementation will ignore slices not dividable by 4.
*/
#include "IKern.h"
#include <cmath>


/*
| Constructor.
*/
IKernAVX2::IKernAVX2( int z, float angle )
{
	makeSinTable( z, angle );
	makeCosTable( z, angle );
}

/*
| Destructor.
*/
IKernAVX2::~IKernAVX2( )
{
	delete[ ] pst;
	delete[ ] pct;
}

void
IKernAVX2::makeSinTable( int z, float angle )
{
	int const z025 = z / 4;
	__m128 * tab = new __m128[ z025 ];
	float p = angle / z;

	for( int t = 0; t < z025; t++ )
	{
		int t4 = 4 * t;
		tab[ t ] =
			_mm_set_ps(
				sin( ( t4 + 3 ) * p ),
				sin( ( t4 + 2 ) * p ),
				sin( ( t4 + 1 ) * p ),
				sin( ( t4 + 0 ) * p )
			);
	}
	this->pst = tab;
}

void
IKernAVX2::makeCosTable( int z, float angle )
{
	int const z025 = z / 4;
	float p = angle / z;
	__m128 * tab = new __m128[ z025 ];
	for( int t = 0; t < z025; t++ )
	{
		int t4 = 4 * t;
		tab[ t ] =
			_mm_set_ps(
				cos( ( t4 + 3 ) * p ),
				cos( ( t4 + 2 ) * p ),
				cos( ( t4 + 1 ) * p ),
				cos( ( t4 + 0 ) * p )
			);
	}
	this->pct = tab;
}

void
IKernAVX2::nearest(
	int isize05,       // size of inverse radon field / 2
	int z,             // amount of steps
	int h,             // height of input
	float * rpadSh05,  // padded, filtered input, shifted by half height
	float * slice      // output
)
{
	int const z025 = z / 4;

	__m128i phadv = _mm_set_epi32( 3 * h, 2 * h, h, 0 );
	__m128i onei = _mm_set_epi32( 1, 1, 1, 1 );
	__m128 oneps = _mm_set_ps( 1.0f, 1.0f, 1.0f, 1.0f );

	for( int iy = -isize05; iy < isize05; iy++ )
	{
		__m128 iy128 = _mm_set1_ps( iy );
		for( int ix = -isize05; ix < isize05; ix++ )
		{
			__m128 ix128 = _mm_set1_ps( ix );

			float * p = rpadSh05;
			__m128 const * pc = pct;
			__m128 const * ps = pst;
			float v = 0;
			for( int t = 0; t < z025; t++ )
			{
				__m128i pi =
					_mm_add_epi32(
						_mm_cvtps_epi32(
							_mm_add_ps(
								_mm_mul_ps( *(pc++), ix128 ),
								_mm_mul_ps( *(ps++), iy128 )
							)
						),
						phadv
					);

				__m128 pv  = _mm_i32gather_ps( p, pi, 4 );

				pv = _mm_hadd_ps( pv, pv );
				pv = _mm_hadd_ps( pv, pv );

				v += reinterpret_cast<float*>( & pv )[ 0 ];
				p += 4 * h;
			}

			*(slice++) = v;
		}
	}
}


void
IKernAVX2::linear(
	int isize05,       // size of inverse radon field / 2
	int z,             // amount of steps
	int h,             // height of input
	float * rpadSh05,  // padded, filtered input, shifted by half height
	float * slice      // output
)
{
	int const z025 = z / 4;

	__m128i phadv = _mm_set_epi32( 3 * h, 2 * h, h, 0 );
	__m128i onei = _mm_set_epi32( 1, 1, 1, 1 );
	__m128 oneps = _mm_set_ps( 1.0f, 1.0f, 1.0f, 1.0f );

	for( int iy = -isize05; iy < isize05; iy++ )
	{
		__m128 iy128 = _mm_set1_ps( iy );

		for( int ix = -isize05; ix < isize05; ix++ )
		{
			__m128 ix128 = _mm_set1_ps( ix );

			float * p = rpadSh05;
			__m128 const * pc = pct;
			__m128 const * ps = pst;
			float v = 0;

			for( int t = 0; t < z025; t++ )
			{
				__m128 pf =
					_mm_add_ps(
						_mm_mul_ps( *(pc++), ix128 ),
						_mm_mul_ps( *(ps++), iy128 )
					);

				__m128i pi =
					_mm_add_epi32(
						_mm_cvttps_epi32( pf ),
						phadv
					);

				__m128 pv  = _mm_i32gather_ps( p, pi, 4 );
				pi = _mm_add_epi32( pi, onei );
				__m128 pv2 = _mm_i32gather_ps( p, pi, 4 );

				__m128 w = _mm_sub_ps( pf, _mm_floor_ps( pf ) );

				pv =
					_mm_add_ps(
						_mm_mul_ps(
							_mm_sub_ps( oneps, w ),
							pv
						),
						_mm_mul_ps( w, pv2 )
					);

				pv = _mm_hadd_ps( pv, pv );
				pv = _mm_hadd_ps( pv, pv );

				v += reinterpret_cast<float*>( & pv )[ 0 ];
				p += 4 * h;
			}

			*(slice++) = v;
		}
	}
}
