#ifndef IKERN_H
#define IKERN_H

#include<nmmintrin.h>
#include<immintrin.h>

/*
| Common super class for all computation kernels.
*/
class IKern
{
public:
	enum kernel {
		KERNEL_AUTO,
		KERNEL_PLAIN,
		KERNEL_SSE42,
		KERNEL_AVX2
	};

	IKern( ) = default;
	virtual ~IKern( ) = default;

	// idenfities the kernel
	virtual enum kernel kernel( ) = 0;

	virtual void nearest(
		int isize05,       // size of inverse radon field / 2
		int z,             // amount of steps
		int h,             // height of input
		float * rpadSh05,  // padded, filtered input, shifted by half height
		float * slice      // output
	) = 0;

	virtual void linear(
		int isize05,       // size of inverse radon field / 2
		int z,             // amount of steps
		int h,             // height of input
		float * rpadSh05,  // padded, filtered input, shifted by half height
		float * slice      // output
	) = 0;
};


/*
| Just plain calculation without any tricks.
*/
class IKernPlain : public IKern
{
public:
	IKernPlain( int z, float angle );
	virtual ~IKernPlain( );

	virtual void nearest( int isize05, int z, int h, float * rpadSh05, float * slice ) override;
	virtual void linear( int isize05, int z, int h, float * rpadSh05, float * slice ) override;

	// idenfities the kernel
	virtual enum kernel kernel( ) override { return KERNEL_PLAIN; }

private:
	void makeSinTable( int z, float angle );
	void makeCosTable( int z, float angle );

	float const * pct;
	float const * pst;
};


/*
| Calculation with SSE42 instructions.
*/
class IKernSSE42 : public IKern
{
public:
	IKernSSE42( int z, float angle );
	virtual ~IKernSSE42( );

	virtual void nearest( int isize05, int z, int h, float * rpadSh05, float * slice ) override;
	virtual void linear( int isize05, int z, int h, float * rpadSh05, float * slice ) override;

	// idenfities the kernel
	virtual enum kernel kernel( ) override { return KERNEL_SSE42; }

private:
	void makeSinTable( int z, float angle );
	void makeCosTable( int z, float angle );

	__m128 const * pct;
	__m128 const * pst;
};


/*
| Calculation with SSE42 and AVX2 instructions.
*/
class IKernAVX2 : public IKern
{
public:
	IKernAVX2( int z, float angle );
	virtual ~IKernAVX2( );

	virtual void nearest( int isize05, int z, int h, float * rpadSh05, float * slice ) override;
	virtual void linear( int isize05, int z, int h, float * rpadSh05, float * slice ) override;

	// idenfities the kernel
	virtual enum kernel kernel( ) override { return KERNEL_AVX2; }

private:
	void makeSinTable( int z, float angle );
	void makeCosTable( int z, float angle );

	__m128 const * pct;
	__m128 const * pst;
};


#endif
