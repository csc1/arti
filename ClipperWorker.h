/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#ifndef CLIPPER_WORKER_H
#define CLIPPER_WORKER_H

#include <QObject>
#include <QString>

#include <vtkCommand.h>
#include <vtkImageData.h>
#include <vtkJPEGReader.h>
#include <vtkSmartPointer.h>

/*
| Reads in the full, clipped jpg data for
| the iradon process in a seperated thread.
*/
class ClipperWorker : public QObject, vtkCommand
{
	Q_OBJECT

public:
	ClipperWorker(
		QString &srcPath,                  // path to the source files
		int nSlices,                       // count of the source files
		int every,                         // use every-th slice
		double angle,                      // angle to rotate
		int e0, int e1, int e2, int e3,    // extent to clip of the source
		int o0, int o1,                    // changed origin
		bool invert,                       // if true inverts input
		double prescaling                  // input down scaling
	) :
		srcPath( srcPath ),
		nSlices( nSlices ),
		every( every ),
		angle( angle ),
		e0( e0 ),
		e1( e1 ),
		e2( e2 ),
		e3( e3 ),
		o0( o0 ),
		o1( o1 ),
		invert( invert ),
		prescaling( prescaling ),
		aborting( false )
	{ }

	void abort( );

	// progress callback from VTK
	virtual void Execute( vtkObject *, unsigned long, void * ) override;

	int dataDims[ 3 ] = { 0, };
	// this will be allocated by ClipperWorker
	// but has to be freed in parent Clipper after
	// takin over the result
	unsigned char * data = 0; // the data here when finished

public slots:

	void process( );

signals:
	void finished( );
	void aborted( );
	void progress( double amount );

private:
	QString srcPath;                    // path to the source files
	int nSlices;                        // count of the source files
	int every;                          // use every-th slice
	double angle;                       // rotate by this
	int e0, e1, e2, e3;                 // extent to clip of the source
	int o0, o1;                         // changed origin
	bool invert;                        // if true input is inverted
	double prescaling;                  // if < 1 input is scaled down
	bool aborting;
	vtkSmartPointer<vtkJPEGReader> reader;
};

#endif
