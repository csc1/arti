/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#include <assert.h>
#include <iostream>

#include "ClipperWorker.h"

#include <vtkCallbackCommand.h>
#include <vtkImageChangeInformation.h>
#include <vtkImageClip.h>
#include <vtkImageData.h>
#include <vtkImageLuminance.h>
#include <vtkImageReslice.h>
#include <vtkImageResize.h>
#include <vtkImageShiftScale.h>
#include <vtkJPEGReader.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkTransform.h>


void
ClipperWorker::process( )
{
	// creates a new chain
	// since clipper chain clips on current slice right away
	// and this works in a different thread

	vtkNew<vtkStringArray> filenames;
	nSlices /= every;
	filenames->SetNumberOfValues( nSlices );
	for( int i = 0, f = 1; i < nSlices; f += every, i++ )
	{
		QString ext = QString( "/%1.jpg" ).arg(  f, 4, 10, QChar('0') );
		filenames->SetValue( i, ( srcPath + ext ).toStdString( ) );
	}
	reader = vtkSmartPointer<vtkJPEGReader>::New( );
	reader->SetFileNames( filenames );
	vtkNew<vtkImageClip> clip;

	if( angle != 0 )
	{
		vtkNew<vtkImageReslice> reslice;
		reslice->SetInputConnection( reader->GetOutputPort( ) );
		reslice->SetInterpolationModeToCubic( );
		vtkNew<vtkTransform> transform;
		int imageW = e1 - e0;
		int imageH = e3 - e2;
		transform->Translate( imageW / 2, imageH / 2, 0 );
		transform->RotateWXYZ( angle, 0, 0, 1 );
		transform->Translate( -imageW / 2, -imageH / 2, 0 );
		reslice->SetResliceTransform( transform );
		clip->SetInputConnection( reslice->GetOutputPort( ) );
	}
	else
	{
		clip->SetInputConnection( reader->GetOutputPort( ) );
	}
	clip->SetOutputWholeExtent( e0, e1, e2, e3, 0, nSlices - 1 );

	vtkNew<vtkImageChangeInformation> vChange;
	vChange->SetInputConnection( clip->GetOutputPort( ) );
	vChange->SetOutputExtentStart( o0, o1, 0 );
	vChange->UpdateInformation( );

	vtkNew<vtkImageResize> vResize;
	vResize->SetInputConnection( vChange->GetOutputPort( ) );
	int psw = ( e1 - e0 ) * prescaling;
	int psh = ( e3 - e2 ) * prescaling;
	// round up to 4 devideable
	if( psw % 4 ) psw += 4 - ( psw % 4 );
	if( psh % 4 ) psh += 4 - ( psh % 4 );
	vResize->SetOutputDimensions( psw, psh, nSlices - 1 );

	reader->AddObserver( vtkCommand::ProgressEvent, this );
	vResize->Update( );

	if( aborting )
	{
		emit aborted( );
		return;
	}

	vtkNew<vtkImageShiftScale> vInvert;
	if( vResize->GetOutput( )->GetNumberOfScalarComponents( ) != 1 )
	{
		vtkNew<vtkImageLuminance> lumen;
		lumen->SetInputConnection( vResize->GetOutputPort( ) );
		vInvert->SetInputConnection( lumen->GetOutputPort( ) );
	}
	else
	{
		vInvert->SetInputConnection( vResize->GetOutputPort( ) );
	}

	if( invert )
	{
		vInvert->SetShift( 255 );
		vInvert->SetScale( -1 );
	}

	if( aborting )
	{
		emit aborted( );
		return;
	}

	vInvert->Update( );

	vInvert->GetOutput( )->GetDimensions( dataDims );
	int dSize = dataDims[ 0 ] * dataDims[ 1 ] * dataDims[ 2 ];
	data = new unsigned char[ dSize ]( );
	memcpy(
		data,
		( unsigned char * ) vInvert->GetOutput( )->GetScalarPointer( ),
		dSize * sizeof( unsigned char )
	);
	emit finished( );
}

/*
| Called by VTK callback reporting progress.
*/
void
ClipperWorker::Execute(
	vtkObject * caller,
	unsigned long eventId,
	void * callData
)
{
	if( eventId != vtkCommand::ProgressEvent ) return;
	if( !aborting ) emit progress( reader->GetProgress( ) );
}


/*
| Aborts the current reading progress.
*/
void
ClipperWorker::abort( )
{
	aborting = true;

	// as VTK currently doesn't support aborting
	// the filenames are set to empty instead
	// (and thus the reader fast fails on them)
	vtkNew<vtkStringArray> filenames;
	filenames->SetNumberOfValues( nSlices );
	for( int i = 0; i < nSlices; i++ )
	{
		filenames->SetValue( i, "" );
	}
	reader->SetFileNames( filenames );
}
