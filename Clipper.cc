/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QThread>

#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkImageBlend.h>
#include <vtkImageCanvasSource2D.h>
#include <vtkImageChangeInformation.h>
#include <vtkImageClip.h>
#include <vtkImageData.h>
#include <vtkImageMapToColors.h>
#include <vtkImageReslice.h>
#include <vtkImageShiftScale.h>
#include <vtkImageViewer.h>
#include <vtkJPEGReader.h>
#include <vtkNew.h>
#include <vtkScalarsToColors.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkTransform.h>

#include "ClipperWorker.h"


/*
| Constructor.
*/
Clipper::Clipper( Ui_MainWindow * ui ) :
	ui( ui )
{
	connect( ui->hSplitter, SIGNAL( splitterMoved( int, int ) ), this, SLOT( slotHSplitterMoved( int, int ) ) );
	connect( ui->vSplitter, SIGNAL( splitterMoved( int, int ) ), this, SLOT( slotVSplitterMoved( int, int ) ) );
	connect( ui->slideCurSlice, SIGNAL( valueChanged( int ) ), this, SLOT( slotCurSliceChanged( int ) ) );
	connect( ui->spinCurSlice, SIGNAL( valueChanged( int ) ), this, SLOT( slotCurSliceChanged( int ) ) );
	connect( ui->spinSizeW, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinSizeChanged( int ) ) );
	connect( ui->spinSizeH, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinSizeChanged( int ) ) );
	connect( ui->spinCenterX, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinCenterChanged( int ) ) );
	connect( ui->spinCenterY, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinCenterChanged( int ) ) );
	connect( ui->spinOutputSize, SIGNAL( valueChanged( int ) ), this, SLOT( slotSpinOutputSizeChanged( int ) ) );
	connect( ui->checkHelpers, SIGNAL( stateChanged( int ) ), this, SLOT( slotCheckHelpersChanged( int ) ) );
	connect( ui->checkInvert, SIGNAL( stateChanged( int ) ), this, SLOT( slotCheckInvertChanged( int ) ) );
	connect( ui->dialRotate, SIGNAL( valueChanged( int ) ), this, SLOT( slotDialRotateChanged( int ) ) );
	connect( ui->dialRotate, SIGNAL( keyPressed( int ) ), this, SLOT( slotDialRotateKeyPressed( int ) ) );
	connect( ui->spinRotate, SIGNAL( valueChanged( double ) ), this, SLOT( slotSpinRotateChanged( double ) ) );

	vtkNew<vtkGenericOpenGLRenderWindow> vRenderWindow;
	clipperWidget = ui->vtkClipper;
	clipperWidget->setRenderWindow( vRenderWindow );
	vRenderWindow->SetWindowName( "clipper" );

	vReader = vtkSmartPointer<vtkJPEGReader>::New( );
	vReslice = vtkSmartPointer<vtkImageReslice>::New( );
	vReslice->SetInputConnection( vReader->GetOutputPort( ) );
	vReslice->SetInterpolationModeToCubic( );
	vClip = vtkSmartPointer<vtkImageClip>::New( );
	vClip->SetInputConnection( vReslice->GetOutputPort( ) );
	vClip->ClipDataOn( );
	vImageChange = vtkSmartPointer<vtkImageChangeInformation>::New( );
	vImageChange->SetInputConnection( vClip->GetOutputPort( ) );
	vImageChange->SetOutputExtentStart( 0, 0, 0 );
	vInvert = vtkSmartPointer<vtkImageShiftScale>::New( );
	vInvert->SetInputConnection( vImageChange->GetOutputPort( ) );

	vtkNew<vtkImageMapToColors> vRgbConverter;
	vRgbConverter->SetOutputFormatToRGB( );
	vtkNew<vtkScalarsToColors> lookupTable;
	vRgbConverter->SetLookupTable( lookupTable );
	vRgbConverter->SetInputConnection( vInvert->GetOutputPort( ) );

	vLineDrawing = vtkSmartPointer<vtkImageCanvasSource2D>::New( );
	vLineDrawing->SetNumberOfScalarComponents( 4 );
	vLineDrawing->SetScalarTypeToUnsignedChar( );
	vLineDrawing->SetExtent( 0, 1, 0, 1, 0, 0 );

	vtkNew<vtkImageBlend> vBlend;
	vBlend->AddInputConnection( vRgbConverter->GetOutputPort( ) );
	vBlend->AddInputConnection( vLineDrawing->GetOutputPort( ) );

	vImageViewer = vtkSmartPointer<vtkImageViewer>::New( );
	vImageViewer->SetColorLevel( 255.0 / 2 );
	vImageViewer->SetColorWindow( 255.0 );
	vImageViewer->SetRenderWindow( vRenderWindow );
	vImageViewer->SetInputConnection( vBlend->GetOutputPort( ) );

	vtkNew<QVTKInteractor> interactor;
	interactor->SetRenderWindow( vRenderWindow );
	interactor->SetInteractorStyle( this );
	interactor->Initialize( );
}

/*
| Destructor.
*/
Clipper::~Clipper( )
{
	if( data ){ delete[ ] data; data = 0; }
}


/*
| Initializes the clipper.
|
| srcPath ... the path to read the images from.
| nSlices ... amount of slices expected there.
*/
void
Clipper::init( QString const & srcPath, int nSlices )
{
	this->nSlices = nSlices;
	this->srcPath = srcPath;

	vtkNew<vtkStringArray> filenames;
	filenames->SetNumberOfValues( nSlices );
	for( int i = 0; i < nSlices; i++ )
	{
		QString ext = QString( "/%1.jpg" ).arg(  i + 1, 4, 10, QChar('0') );
		filenames->SetValue( i, ( srcPath + ext ).toStdString( ) );
	}
	vReader->SetFileNames( filenames );
	vReader->UpdateInformation( );

	{
		int * imgExtent = vReader->GetDataExtent( );
		imageW = imgExtent[ 1 ];
		imageH = imgExtent[ 3 ];
		ui->labelMaxSlices->setText( QString( "/ %1" ).arg( nSlices ) );

		ui->spinCurSlice->blockSignals( true );
		ui->spinCurSlice->setValue( 1 );
		ui->spinCurSlice->setMaximum( nSlices );
		ui->spinCurSlice->blockSignals( false );

		ui->slideCurSlice->blockSignals( true );
		ui->slideCurSlice->setValue( 1 );
		ui->slideCurSlice->setMaximum( nSlices );
		ui->slideCurSlice->blockSignals( false );

		ui->spinSizeW->blockSignals( true );
		ui->spinSizeW->setMaximum( imageW * 2 );
		ui->spinSizeW->blockSignals( false );

		ui->spinSizeH->blockSignals( true );
		ui->spinSizeH->setMaximum( imageH * 2 );
		ui->spinSizeH->blockSignals( false );

		ui->spinCenterX->blockSignals( true );
		ui->spinCenterX->setMaximum( imageW * 2 );
		ui->spinCenterX->blockSignals( false );

		ui->spinCenterY->blockSignals( true );
		ui->spinCenterY->setMaximum( imageH * 2 );
		ui->spinCenterY->blockSignals( false );
	}
	originX = -imageW / 2;
	originY = -imageH / 2;

	vtkNew<vtkTransform> transform;
	vReslice->SetResliceTransform( transform );

	vClip->SetOutputWholeExtent( 0, imageW, 0, imageH, 0, 0 );
	vImageChange->SetOutputExtentStart( 0, 0, 0 );

	if( ui->checkInvert->checkState( ) )
	{
		vInvert->SetShift( -255.0 );
		vInvert->SetScale( -1.0 );
	}
	else
	{
		vInvert->SetShift( 0.0 );
		vInvert->SetScale( 1.0);
	}

	loaded = true;

	//setClipperSize( 927, 719 );
	//originX = -2035;
	//originY = -1860;
	setClipperSize( 250, 250 );
	update( );
}

/*
| Character key is pressed.
| This does two things at once.
| 1: disables vtk's default key handling on imageViewer
| 2: replaces it with quick access keys
*/
void
Clipper::OnChar( )
{
	char * key = this->Interactor->GetKeySym( );
	switch( *key )
	{
		case '1' :  ui->spinCurSlice->setValue( 1 ); break;
		case '2' :  ui->spinCurSlice->setValue( nSlices / 4 ); break;
		case '3' :  ui->spinCurSlice->setValue( nSlices / 2 ); break;
		case '4' :  ui->spinCurSlice->setValue( nSlices * 3 / 4 ); break;
		case '5' :  ui->spinCurSlice->setValue( nSlices ); break;
	}
}

void
Clipper::OnLeftButtonDown( )
{
	if( rightMousePressed ) return;
	leftMousePressed = true;
	pressStartX = this->Interactor->GetEventPosition( )[ 0 ];
	pressStartY = this->Interactor->GetEventPosition( )[ 1 ];
}

void
Clipper::OnLeftButtonUp( )
{
	if( !leftMousePressed ) return;
	leftMousePressed = false;
	int x = this->Interactor->GetEventPosition( )[ 0 ];
	int y = this->Interactor->GetEventPosition( )[ 1 ];
	leftMouseAction( x, y, true );
}

void
Clipper::OnRightButtonDown( )
{
	if( leftMousePressed ) return;
	rightMousePressed = true;
	pressStartX = this->Interactor->GetEventPosition( )[ 0 ];
	pressStartY = this->Interactor->GetEventPosition( )[ 1 ];
}

void
Clipper::OnRightButtonUp( )
{
	if( !rightMousePressed ) return;
	rightMousePressed = false;
	int x = this->Interactor->GetEventPosition( )[ 0 ];
	int y = this->Interactor->GetEventPosition( )[ 1 ];
	rightMouseAction( x, y, true );
}

/*
| Called when mouse moved over the clipper area.
*/
void
Clipper::OnMouseMove( )
{
	int x = this->Interactor->GetEventPosition( )[ 0 ];
	int y = this->Interactor->GetEventPosition( )[ 1 ];
	if( leftMousePressed) leftMouseAction( x, y, false );
	if( rightMousePressed ) rightMouseAction( x, y, false );
}

/*
| Handles the left mouse action ( changing the origin/positing )
*/
void
Clipper::leftMouseAction( int x, int y, bool release )
{
	x += originX - pressStartX;
	y += originY - pressStartY;

	if( x > 0 ) x = 0;
	if( y > imageH ) y = imageH;
	if( x < -imageW ) x = -imageW;
	if( y < -imageH ) y = -imageH;

	updateVtkClipper( x, y, zSlice );
	if( release ){ originX = x; originY = y; }
}

/*
| Handles the right mouse action ( changing the z-slice )
*/
void
Clipper::rightMouseAction( int x, int y, bool release )
{
	int z = zSlice;
	z += ( x - pressStartX  ) / 3;
	if( z < 0 ) z = 0;
	if( z > nSlices - 1 ) z = nSlices - 1;
	clipperWidget->renderWindow( )->Render( );
	if( release ) zSlice = z;
	updateVtkClipper( originX, originY, z );
}

/*
| Updates the clippper
| Redraws the vertical, red line.
|
| ox / oy ... originx/y for image
| oz      ... zSlice
*/
void
Clipper::updateVtkClipper( int ox, int oy, int oz )
{
	int w = ui->hSplitter->sizes( )[ 1 ];
	int h = ui->vSplitter->sizes( )[ 1 ];
	int w05 = w / 2 + 1;
	int h05 = h / 2 + 1;

	vLineDrawing->SetExtent( 0, w, 0, h, 0, 0 );
	vLineDrawing->SetDrawColor( 0.0, 0.0, 0.0, 0.0 );
	vLineDrawing->FillBox( 0, w, 0, h );
	vLineDrawing->SetDrawColor( 255.0, 0.0, 0.0, 192.0 );
	vLineDrawing->DrawSegment( w05, 0, w05, h );

	if( ui->checkHelpers->checkState( ) )
	{
		for( int i = 1; i < 19; i++ )
		{
			if( w05 - i < 0 ) continue;
			vLineDrawing->SetDrawColor( 0.0, 255.0, 0.0, 190.0 - i * 10 );
			vLineDrawing->DrawSegment( w05 + i * 10, 0, w05 + i * 10, h );
			vLineDrawing->DrawSegment( w05 - i * 10, 0, w05 - i * 10, h );
		}
	}

	ox += w05;
	oy += h05;
	vClip->SetOutputWholeExtent( -ox, -ox + w, -oy, -oy + h, oz, oz );
	int ex = w05 - w / 2;
	int ey = h05 - h / 2;
	if( ox > 0 ) ex += ox;
	if( oy > 0 ) ey += oy;
	vImageChange->SetOutputExtentStart( ex, ey, 0 );
	clipperWidget->renderWindow( )->Render( );

	ui->spinCurSlice->blockSignals( true );
	ui->slideCurSlice->blockSignals( true );
	ui->spinSizeW->blockSignals( true );
	ui->spinSizeH->blockSignals( true );
	ui->spinCenterX->blockSignals( true );
	ui->spinCenterY->blockSignals( true );

	ui->spinCurSlice->setValue( oz + 1 );
	ui->slideCurSlice->setValue( oz + 1 );
	ui->spinSizeW->setValue( w );
	ui->spinSizeH->setValue( h );
	ui->spinCenterX->setValue( -ox + w05 );
	ui->spinCenterY->setValue( -oy + h05 );

	ui->spinCenterY->blockSignals( false );
	ui->spinCenterX->blockSignals( false );
	ui->spinSizeH->blockSignals( false );
	ui->spinSizeW->blockSignals( false );
	ui->slideCurSlice->blockSignals( false );
	ui->spinCurSlice->blockSignals( false );

	if( !outputSizeUserSet )
	{
		ui->spinOutputSize->blockSignals( true );
		int v = ( 2.0 * w05 ) / sqrt( 2.0 );
		v -= v % 4;
		ui->spinOutputSize->setValue( v );
		ui->spinOutputSize->blockSignals( false );
	}
}


/*
| Common code for splitter moval (horizonal and vertical).
|
| splitter ... pointer to the moved splitter
| pos      ... position the splitter moved to
| index    ... the part of the splitter being moved
|
| Returns the inner size of the splitter
*/
int
Clipper::splitterMoved( QSplitter * splitter, int pos, int index )
{
	QList<int> sizes = splitter->sizes( );
	int total = sizes[ 0 ] + sizes[ 1 ] + sizes[ 2 ];
	if( index == 1 )
	{
		sizes[ 2 ] = sizes[ 0 ];
		sizes[ 1 ] = total - sizes[ 2 ] - sizes[ 0 ];
		if( sizes[ 1 ] % 2 == 0 ){ sizes[ 1 ]++; sizes[ 2 ]--; }
	}
	else if( index == 2 )
	{
		sizes[ 0 ] = sizes[ 2 ];
		sizes[ 1 ] = total - sizes[ 2 ] - sizes[ 0 ];
		if( sizes[ 1 ] % 2 == 0 ){ sizes[ 1 ]++; sizes[ 0 ]--; }
	}

	if( sizes[ 1 ] < 2 )
	{
		sizes[ 1 ] = ( total % 2 ) ? 3 : 2;
		sizes[ 0 ] = ( total - sizes[ 1 ] ) / 2;
		sizes[ 2 ] = sizes[ 0 ];
	}

	splitter->blockSignals( true );
	splitter->setSizes( sizes );
	splitter->blockSignals( false );
	updateVtkClipper( originX, originY, zSlice );
	return sizes[ 1 ];
}

/*
| The horiziontal splitter moved.
*/
void
Clipper::slotHSplitterMoved( int pos, int index )
{
	clipW = splitterMoved( ui->hSplitter, pos, index );
}

/*
| The vertical splitter moved.
*/
void
Clipper::slotVSplitterMoved( int pos, int index )
{
	clipH = splitterMoved( ui->vSplitter, pos, index );
}

/*
| The current slice changed (slider or spinbox)
*/
void
Clipper::slotCurSliceChanged( int val )
{
	zSlice = val - 1;
	updateVtkClipper( originX, originY, zSlice );
}

/*
| One of the size spins changed.
*/
void
Clipper::slotSpinSizeChanged( int )
{
	int w = ui->spinSizeW->value( );
	int h = ui->spinSizeH->value( );
	setClipperSize( w, h );
	updateVtkClipper( originX, originY, zSlice );
}

/*
| One of the center value spins changed.
*/
void
Clipper::slotSpinCenterChanged( int )
{
	originX = -ui->spinCenterX->value( );
	originY = -ui->spinCenterY->value( );
	updateVtkClipper( originX, originY, zSlice );
}

/*
| The "helpers" chechbox changed.
*/
void
Clipper::slotCheckHelpersChanged( int )
{
	updateVtkClipper( originX, originY, zSlice );
}

/*
| Sets the clipper size to width/height.
*/
void
Clipper::setClipperSize( int w, int h )
{
	clipW = w;
	clipH = h;
	QSplitter * splitter = ui->hSplitter;
	QList<int> sizes = splitter->sizes( );
	int total = sizes[ 0 ] + sizes[ 1 ] + sizes[ 2 ];
	if( total < w ) w = total - 1;
	if( w % 2 == 0 ) w++;
	sizes[ 1 ] = w;
	sizes[ 0 ] = ( total - sizes[ 1 ] ) / 2;
	sizes[ 2 ] = total - sizes[ 1 ] - sizes[ 0 ];

	splitter->blockSignals( true );
	splitter->setSizes( sizes );
	splitter->blockSignals( false );

	splitter = ui->vSplitter;
	sizes = splitter->sizes( );
	total = sizes[ 0 ] + sizes[ 1 ] + sizes[ 2 ];
	if( total < h ) h = total - 1;
	if( h % 2 == 0 ) h++;
	sizes[ 1 ] = h;
	sizes[ 0 ] = ( total - sizes[ 1 ] ) / 2;
	sizes[ 2 ] = total - sizes[ 1 ] - sizes[ 0 ];

	splitter->blockSignals( true );
	splitter->setSizes( sizes );
	splitter->blockSignals( false );

	ui->spinSizeW->blockSignals( true );
	ui->spinSizeH->blockSignals( true );

	ui->spinSizeW->setValue( w );
	ui->spinSizeH->setValue( h );

	ui->spinSizeH->blockSignals( false );
	ui->spinSizeW->blockSignals( false );
}

/*
| Updates the clipper.
*/
void
Clipper::update( )
{
	if( !loaded ) return;
	updateVtkClipper( originX, originY, zSlice );
	setClipperSize( clipW, clipH );
}

void
Clipper::slotCheckInvertChanged( int )
{
	if( ui->checkInvert->checkState( ) )
	{
		vInvert->SetShift( -255.0 );
		vInvert->SetScale( -1.0 );
	}
	else
	{
		vInvert->SetShift( 0.0 );
		vInvert->SetScale( 1.0);
	}
	clipperWidget->renderWindow( )->Render( );
}

void
Clipper::requestData( int every )
{
	if( data ){ delete[ ] data; data = 0; }

	int * ex = vClip->GetOutputWholeExtent();
	int * os = vImageChange->GetOutputExtentStart( );

	if( thread )
	{
		if( thread->isRunning( ) )
		{
			std::cerr << "clipper thread still running ignoring start request" << endl;
			emit aborted( );
			return;
		}
		thread->deleteLater( );
	}
	if( worker ) worker->deleteLater( );

	thread = new QThread;
	worker =
		new ClipperWorker(
			srcPath,
			nSlices,
			every,
			ui->spinRotate->value( ),
			ex[ 0 ], ex[ 1 ], ex[ 2 ], ex[ 3 ],
			os[ 0 ], os[ 1 ],
			ui->checkInvert->checkState( ),
			ui->spinPrescaling->value( )
		);
	worker->moveToThread( thread );

	connect( thread, SIGNAL( started( ) ), worker, SLOT( process( ) ) );
	connect( worker, SIGNAL( finished( ) ), thread, SLOT( quit( ) ) );
	connect( worker, SIGNAL( finished( ) ), this, SLOT( slotWorkerFinished( ) ) );
	connect( worker, SIGNAL( aborted( ) ), thread, SLOT( quit( ) ) );
	connect( worker, SIGNAL( aborted( ) ), this, SLOT( slotWorkerAborted( ) ) );
	connect( worker, SIGNAL( progress( double ) ), this, SLOT( slotWorkerProgress( double ) ) );

	// calling deleteLater right on thread exit causes VTK to hick up...
	// better just letting it linger for a little while

	thread->start( );
}

/*
| Aborts reading data.
*/
void
Clipper::abortData( )
{
	worker->abort( );
}

/*
| Progress of the reader worker.
*/
void
Clipper::slotWorkerProgress( double amount )
{
	emit progress( amount );
}

/*
| The worker finished.
*/
void
Clipper::slotWorkerAborted( )
{
	delete[ ] worker->data; worker->data = 0;
//	delete worker; worker = 0;
//	delete thread; thread = 0;
	emit aborted( );
}

/*
| The worker finished.
*/
void
Clipper::slotWorkerFinished( )
{
	data = worker->data;
	dataDims[ 0 ] = worker->dataDims[ 0 ];
	dataDims[ 1 ] = worker->dataDims[ 1 ];
	dataDims[ 2 ] = worker->dataDims[ 2 ];
//	delete worker; worker = 0;
//	worker->deleteLater( ); worker = 0;
//	delete thread; thread = 0;
	emit finished( );
}

void
Clipper::slotSpinOutputSizeChanged( int value )
{
	outputSizeUserSet = true;
}

void
Clipper::slotDialRotateChanged( int value )
{
	ui->spinRotate->blockSignals( true );
	ui->spinRotate->setValue( value / 100.0 );
	ui->spinRotate->blockSignals( false );
	updateRotate( value / 100.0 );
}

void
Clipper::slotDialRotateKeyPressed( int code )
{
	switch( code )
	{
		case Qt::Key_1 : ui->spinCurSlice->setValue( 1 ); break;
		case Qt::Key_2 : ui->spinCurSlice->setValue( nSlices / 4 ); break;
		case Qt::Key_3 : ui->spinCurSlice->setValue( nSlices / 2 ); break;
		case Qt::Key_4 : ui->spinCurSlice->setValue( nSlices * 3 / 4 ); break;
		case Qt::Key_5 : ui->spinCurSlice->setValue( nSlices ); break;
	}
}

void
Clipper::slotSpinRotateChanged( double value )
{
	ui->dialRotate->blockSignals( true );
	ui->dialRotate->setValue( value * 100.0 );
	ui->dialRotate->blockSignals( false );
	updateRotate( value );
}

void
Clipper::updateRotate( double angle )
{
	vtkNew<vtkTransform> transform;
	if( angle != 0 )
	{
		transform->Translate( imageW / 2, imageH / 2, 0 );
		transform->RotateWXYZ( angle, 0, 0, 1 );
		transform->Translate( -imageW / 2, -imageH / 2, 0 );
	}
	vReslice->SetResliceTransform( transform );
	clipperWidget->renderWindow( )->Render( );
}
