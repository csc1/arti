/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#include <QKeyEvent>
#include "QKeyDial.h"
#include <iostream>

using namespace std;

void
QKeyDial::keyPressEvent( QKeyEvent *event )
{
	QDial::keyPressEvent( event );
	emit keyPressed( event->key( ) );
}

