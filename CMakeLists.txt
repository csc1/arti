cmake_minimum_required(VERSION 3.3 FATAL_ERROR)
project( arti VERSION "1.0.0" )

#vtkRenderingContextOpenGL2
find_package(VTK COMPONENTS
	vtkCommonColor
	vtkCommonCore
	vtkCommonDataModel
	vtkFiltersSources
	vtkImagingCore
	vtkImagingColor
	vtkImagingSources
	vtkInteractionImage
	vtkInteractionStyle
	vtkIOCore
	vtkIOExport
	vtkIOImage
	vtkIOXML
	vtkRenderingCore
	vtkRenderingFreeType
	vtkRenderingGL2PSOpenGL2
	vtkRenderingOpenGL2
	vtkRenderingVolume
	vtkRenderingVolumeOpenGL2
	vtkViewsQt
)

message( STATUS "VTK_VERSION: ${VTK_VERSION}" )

if (VTK_VERSION VERSION_LESS "8.90.0")
	# Instruct CMake to run moc automatically when needed.
	set(CMAKE_AUTOMOC ON)
else( )
	# Instruct CMake to run moc automatically when needed.
	set(CMAKE_AUTOMOC ON)
	set(CMAKE_AUTOUIC ON)
endif( )

find_package( Qt5 COMPONENTS Widgets REQUIRED )
find_package( PkgConfig REQUIRED )

configure_file( config.h.in config.h )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR} )

set( ARTI_SOURCES
	Clipper.cc
	ClipperWorker.cc
	IRadon.cc
	IRadonWorker.cc
	MainWindow.cc
	QKeyDial.cc
	main.cc
	ikern/IKernPlain.cc
	ikern/IKernSSE42.cc
	ikern/IKernAVX2.cc
	kiss_fft130/kiss_fft.c
)

set( ARTI_UI_FILES
	MainWindow.ui
)

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Dkiss_fft_scalar=float -O3" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Dkiss_fft_scalar=float -O3" )
set_source_files_properties( ikern/IKernSSE42.cc PROPERTIES COMPILE_FLAGS "-msse4.2" )
set_source_files_properties( ikern/IKernAVX2.cc PROPERTIES COMPILE_FLAGS "-msse4.2 -mavx2" )

#set( CMAKE_BUILD_TYPE Debug )
#set( CMAKE_CXX_FLAGS_DEBUG "-O0 -g" )
#set( CMAKE_C_FLAGS_DEBUG "-O0 -g" )

qt5_wrap_ui( ARTI_UI_SOURCES ${ARTI_UI_FILES} )

if( VTK_VERSION VERSION_LESS "8.90.0" )
	include( ${VTK_USE_FILE} )
	add_executable( arti ${ARTI_SOURCES} ${ARTI_UI_SOURCES} )
	qt5_use_modules( arti Core Gui )
	target_link_libraries( arti ${VTK_LIBRARIES} )
else( )
	add_executable( arti ${ARTI_SOURCES} ${ARTI_UI_SOURCES} )
	qt5_use_modules( arti Core Gui )
	target_link_libraries( arti ${VTK_LIBRARIES} )
	vtk_module_autoinit(
		TARGETS arti
		MODULES ${VTK_LIBRARIES}
	)
endif( )

