/*
| This file is part of arti.
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkImageChangeInformation.h>
#include <vtkImageViewer.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>

#include <QFileSystemModel>
#include <QItemSelection>
#include <QMainWindow>

#include "Clipper.h"
#include "IRadon.h"


/*
| The window in which everything happens.
*/
class Ui_MainWindow;
class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	enum status_e {
		STATUS_SELECT,
		STATUS_CLIP,
		STATUS_READING,
		STATUS_COMPUTING,
		STATUS_DONE
	};

	MainWindow( );
	~MainWindow( );
	status_e status;

public slots:

	void slotButtonAbortClicked( bool );
	void slotButtonStartClicked( bool );
	void slotButtonUseClicked( bool );
	void slotButtonSaveMhdClicked( bool );
	void slotButtonSaveVtiClicked( bool );
	void slotButtonSaveJsonClicked( bool );
	void slotSpinUseSliceChanged( int );
	void slotSpinThreadsChanged( int );
	void slotSpinZStackChanged( int );
	void slotSpinZ1XChanged( int );
	void slotSpinZ2XChanged( int );
	void slotSpinZ2YChanged( double );
	void slotSliderZStackChanged( int );
	void slotSliderZ1XChanged( int );
	void slotSliderZ2XChanged( int );
	void slotSliderZ2YChanged( int );
	void slotExit();
	void slotIRadonProgress( int );
	void slotReadProgress( double );
	void slotClipperAborted( );
	void slotClipperFinished( );
	void slotIRadonAborted( );
	void slotIRadonFinished( );
	void slotSelectionChanged( const QItemSelection &, const QItemSelection & );
	void slotTabSelected( int );

private:

	Ui_MainWindow * ui;

	// input model of the input picker
	QFileSystemModel * inputModel;

	// the iradon engine
	IRadon iradon;

	// the clipper handler
	Clipper * clipper;

	// currently selected source
	QString selectionBase;
	int selectionCount;

	// size of the created result
	int resultSize;

	void resizeEvent( QResizeEvent* event ) override;
	void changeEvent( QEvent* evt ) override;

	vtkSmartPointer<vtkPiecewiseFunction> vPFuncRay;
	vtkSmartPointer<vtkImageViewer> vImageViewerZStack;
	vtkSmartPointer<vtkImageChangeInformation> vImageChangeZStack;
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> vRenderWindowZStack;
	vtkSmartPointer<vtkVolume> vVolume;
	vtkSmartPointer<vtkRenderer> vRendererRay;
	vtkSmartPointer<vtkFixedPointVolumeRayCastMapper> vVolumeMapper;
	void updateRayFunc( );
	void updateZStack( );
};

#endif
